require "application_system_test_case"

class LocationsTest < ApplicationSystemTestCase
  setup do
    @location = locations(:one)
  end

  test "visiting the index" do
    visit locations_url
    assert_selector "h1", text: "Locations"
  end

  test "creating a Location" do
    visit locations_url
    click_on "New Location"

    fill_in "Address", with: @location.address
    fill_in "Avg rate", with: @location.avg_rate
    fill_in "City", with: @location.city
    fill_in "Desc", with: @location.desc
    fill_in "Lat", with: @location.lat
    fill_in "Long", with: @location.long
    fill_in "Nbchambre", with: @location.nbChambre
    fill_in "Nbcouchage", with: @location.nbCouchage
    fill_in "Photos", with: @location.photos
    fill_in "Price", with: @location.price
    fill_in "Street", with: @location.street
    fill_in "Titre", with: @location.titre
    fill_in "V type", with: @location.v_type
    fill_in "Zip", with: @location.zip
    click_on "Create Location"

    assert_text "Location was successfully created"
    click_on "Back"
  end

  test "updating a Location" do
    visit locations_url
    click_on "Edit", match: :first

    fill_in "Address", with: @location.address
    fill_in "Avg rate", with: @location.avg_rate
    fill_in "City", with: @location.city
    fill_in "Desc", with: @location.desc
    fill_in "Lat", with: @location.lat
    fill_in "Long", with: @location.long
    fill_in "Nbchambre", with: @location.nbChambre
    fill_in "Nbcouchage", with: @location.nbCouchage
    fill_in "Photos", with: @location.photos
    fill_in "Price", with: @location.price
    fill_in "Street", with: @location.street
    fill_in "Titre", with: @location.titre
    fill_in "V type", with: @location.v_type
    fill_in "Zip", with: @location.zip
    click_on "Update Location"

    assert_text "Location was successfully updated"
    click_on "Back"
  end

  test "destroying a Location" do
    visit locations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Location was successfully destroyed"
  end
end
