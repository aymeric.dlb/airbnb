require "application_system_test_case"

class EquipementsTest < ApplicationSystemTestCase
  setup do
    @equipement = equipements(:one)
  end

  test "visiting the index" do
    visit equipements_url
    assert_selector "h1", text: "Equipements"
  end

  test "creating a Equipement" do
    visit equipements_url
    click_on "New Equipement"

    fill_in "Nom", with: @equipement.nom
    click_on "Create Equipement"

    assert_text "Equipement was successfully created"
    click_on "Back"
  end

  test "updating a Equipement" do
    visit equipements_url
    click_on "Edit", match: :first

    fill_in "Nom", with: @equipement.nom
    click_on "Update Equipement"

    assert_text "Equipement was successfully updated"
    click_on "Back"
  end

  test "destroying a Equipement" do
    visit equipements_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Equipement was successfully destroyed"
  end
end
