Rails.application.routes.draw do
  resources :conversations do
    resource :conversation_user
    resources :messages
  end
  resources :reservations, path_names: { new: 'new/:location_id' }
  resources :favoris, path_names: { new: 'new/:location_id' }
  resources :avis, path_names: { new: 'new/:location_id' }
  resources :categories
  resources :equipements, path_names: { new: 'new/:location_id' }
  devise_for :users
  resources :locations
  resources :searches
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: "locations#index"
end
