# Installation

Télécharger et installer **Ruby+devkit version 2.6.8** https://rubyinstaller.org/downloads/archives/

Télécharger et installer **Cygwin** https://cygwin.com/install.html

Dans le command promp de ruby, installer rails : 
```bash
gem install rails
```

Dans le projet, avec l'invite de commande, éxécuter  :
```bash
bundle install
yarn install --check-files
rails webpacker:install
rails s
```

Installer et éxécuter **maildev** via l'invite de commande : 
```bash
npm install -g maildev
maildev
```
