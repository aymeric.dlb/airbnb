class Location < ApplicationRecord
  def self.search(search)
    if search
      where(["titre LIKE ?","%#{search}%"])
    else
      all
    end
  end
end
