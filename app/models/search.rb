class Search < ApplicationRecord
  def search_locations
    locations = Location.all

    locations = locations.where(["titre LIKE ?","%#{keywords}%"]) if keywords.present?
    locations = locations.where(["v_type LIKE ?", v_type]) if v_type.present?

    locations = locations.where(["zip LIKE ?", zip]) if zip.present?
    locations = locations.where(["city LIKE ?", "%#{city}%"]) if city.present?


    locations = locations.where(["price >= ?", min_price]) if min_price.present?
    locations = locations.where(["price <= ?", max_price]) if max_price.present?

    locations = locations.where(["nbChambre >= ?", min_nbChambre]) if min_nbChambre.present?
    locations = locations.where(["nbChambre <= ?", max_nbChambre]) if max_nbChambre.present?

    locations = locations.where(["nbCouchage >= ?", min_nbCouchage]) if min_nbCouchage.present?
    locations = locations.where(["nbCouchage <= ?", max_nbCouchage]) if max_nbCouchage.present?

    return locations
  end
end
