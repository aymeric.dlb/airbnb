json.extract! avi, :id, :note, :comment, :created_at, :updated_at
json.url avi_url(avi, format: :json)
