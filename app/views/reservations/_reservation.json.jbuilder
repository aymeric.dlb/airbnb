json.extract! reservation, :id, :datedeb, :datefin, :nbpersonnes, :price, :status, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)
