json.extract! location, :id, :titre, :nbChambre, :nbCouchage, :desc, :v_type, :address, :street, :zip, :city, :lat, :long, :price, :photos, :avg_rate, :created_at, :updated_at
json.url location_url(location, format: :json)
