class EquipementsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_equipement, only: %i[ show edit update destroy ]

  # GET /equipements or /equipements.json
  def index
    @equipements = Equipement.all
    @categories = Category.all
  end

  # GET /equipements/1 or /equipements/1.json
  def show
  end

  # GET /equipements/new
  def new
    @equipement = Equipement.new
    @categories = Category.all
  end

  # GET /equipements/1/edit
  def edit
    @categories = Category.all
  end

  # POST /equipements or /equipements.json
  def create
    @equipement = Equipement.new(equipement_params)
    @categorie = Category.find(@equipement.categories_id)
    @equipement.location_id = params[:location_id]

    respond_to do |format|
      if @equipement.save
        @location = Location.find(params[:location_id])

        format.html { redirect_to @location, notice: "L'Equipement a bien été créé." }
        format.json { render :show, status: :created, location: @location }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @equipement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /equipements/1 or /equipements/1.json
  def update
    respond_to do |format|
      if @equipement.update(equipement_params)
        @location = Location.find(@equipement.location_id)
        format.html { redirect_to  @location, notice: "L'Equipement a bien été modifié." }
        format.json { render :show, status: :ok, location: @location }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @equipement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /equipements/1 or /equipements/1.json
  def destroy
    @location = Location.find(@equipement.location_id)
    @equipement.destroy

    respond_to do |format|
      format.html { redirect_to @location, notice: "L'Equipement a été supprimé." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_equipement
      @equipement = Equipement.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def equipement_params
      params.require(:equipement).permit(:nom, :categories_id, :location_id)
    end
end
