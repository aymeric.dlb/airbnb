class ConversationUsersController < ApplicationController
    before_action :authenticate_user!
    before_action :set_conversation

    def create
        @conversation.conversation_users.where(user: current_user).first_or_create
        redirect_to @conversation
    end

    def destroy
        @conversation.conversation_users.where(user: current_user).destroy_all
        redirect_to @conversation
    end

    private

        def set_conversation
            @conversation = Conversation.find(params[:conversation_id])
        end
    end