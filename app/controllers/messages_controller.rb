class messagesController < ApplicationController
    before_action :authenticate_user!
    before_action :set_conversation
end