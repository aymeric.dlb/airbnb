class ReservationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_reservation, only: %i[ show edit update destroy ]

  # GET /reservations or /reservations.json
  def index
    @reservations = Reservation.where(user_id: current_user.id)
    @locations = Location.all
  end

  # GET /reservations/1 or /reservations/1.json
  def show
    @location = Location.find(@reservation.location_id)
    @user = User.where(id: @reservation.user_id)
  end

  # GET /reservations/new
  def new
    @reservation = Reservation.new
  end

  # GET /reservations/1/edit
  def edit
    @location = Location.find(@reservation.location_id)
    @user = User.where(id: @reservation.user_id)
  end

  # POST /reservations or /reservations.json
  def create
    @reservation = Reservation.new(reservation_params)
    @reservation.user_id = current_user.id
    @reservation.status = "En attente"

    # CALCUL DU PRIX DE LA RESERVATION (nbNuits*prix + taxeSejour*nbPersonne*nbNuits)
    @location = Location.find(@reservation.location_id)
    @reservation.price = ((@reservation.datefin - @reservation.datedeb).to_i * @location.price) + @location.taxesjr * @reservation.nbpersonnes * (@reservation.datefin - @reservation.datedeb).to_i


    @reservation_conflict = false

    @location_reservations = Reservation.where(location_id: @reservation.location_id).where(status: ["Acceptée", "Payée", "Terminée"])
    @location_reservations.each do |location_reservation|
      if @reservation.datedeb.between?(location_reservation.datedeb, location_reservation.datefin) || @reservation.datefin.between?(location_reservation.datedeb, location_reservation.datefin)
        @reservation_conflict = true
      end
    end

    respond_to do |format|

      if @reservation.datefin <= @reservation.datedeb

        format.html { redirect_to @location, alert: "La date de fin de réservation doit être après la date de début !" }
        format.json { render :show, status: :unprocessable_entity, location: @location }

      elsif @reservation_conflict
        format.html { redirect_to @location, alert: "Il y a déjà une réservation acceptée pour cette période" }
        format.json { render :show, status: :unprocessable_entity, location: @location }

      else
        if @reservation.save
          format.html { redirect_to @reservation, notice: "Réservation enregistrée avec succès" }
          format.json { render :show, status: :created, location: @reservation }
        else
          format.html { render :new, status: :unprocessable_entity }
          format.json { render json: @reservation.errors, status: :unprocessable_entity }
        end
      end

    end
  end

  # PATCH/PUT /reservations/1 or /reservations/1.json
  def update
    @location = Location.find(@reservation.location_id)

    @reservation_conflict = false

    if reservation_update_params["status"] == "Acceptée" || reservation_update_params["status"] == "Payée" || reservation_update_params["status"] == "Terminée"
      @location_reservations = Reservation.where(location_id: @reservation.location_id).where(status: ["Acceptée", "Payée", "Terminée"])
      @location_reservations.each do |location_reservation|
        if @reservation.datedeb.between?(location_reservation.datedeb, location_reservation.datefin) || @reservation.datefin.between?(location_reservation.datedeb, location_reservation.datefin)
          if @reservation.id != location_reservation.id
            @reservation_conflict = true
          end
        end
      end
    end

    respond_to do |format|

      if @reservation_conflict
        @reservation.errors.add(:base, "Il y a déjà une réservation acceptée pour cette période !")
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }

      else
        if @reservation.update(reservation_update_params)

          if reservation_update_params["status"] == "Acceptée" || reservation_update_params["status"] == "Payée" || reservation_update_params["status"] == "Terminée"
            @location_reservations2 = Reservation.where(location_id: @reservation.location_id)
            @location_reservations2.each do |location_reservation|
              if location_reservation.datedeb.between?(@reservation.datedeb, @reservation.datefin) || location_reservation.datefin.between?(@reservation.datedeb, @reservation.datefin)
                if @reservation.id != location_reservation.id
                  location_reservation.status = "Refusée"
                  location_reservation.save
                end
              end
            end
          end

          format.html { redirect_to @reservation, notice: "La réservation a bien été mise à jour" }
          format.json { render :show, status: :ok, location: @reservation }
        else
          format.html { render :edit, status: :unprocessable_entity }
          format.json { render json: @reservation.errors, status: :unprocessable_entity }
        end
      end

    end
  end

  # DELETE /reservations/1 or /reservations/1.json
  def destroy
    @reservation.destroy
    respond_to do |format|
      format.html { redirect_to reservations_url, notice: "La réservation a bien été supprimée" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def reservation_params
      params.require(:reservation).permit(:datedeb, :datefin, :nbpersonnes, :location_id, :price, :status)
    end

    # Only allow a list of trusted parameters through.
    def reservation_update_params
      params.require(:reservation).permit(:status)
    end
end
