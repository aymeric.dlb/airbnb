class LocationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_location, only: %i[ show edit update destroy ]

  # GET /locations or /locations.json
  def index
    @locations = Location.search(params[:search])
  end

  # GET /locations/1 or /locations/1.json
  def show
    @location_avis = Avi.where(location_id: @location.id)
    @reservation = Reservation.new
    @equipements = Equipement.where(location_id: @location.id)
    @categories = Category.all
    @location_user = User.find(@location.user_id)
    @favori = Favori.new
    @users = User.all
    @location_reservations = Reservation.where(location_id: @location.id)
  end

  # GET /locations/new
  def new
    @location = Location.new
  end

  # GET /locations/1/edit
  def edit
  end

  # POST /locations or /locations.json
  def create
    @location = Location.new(location_params)
    @location.user_id = current_user.id
    @location.avg_rate = 11

    respond_to do |format|
      if @location.save
        format.html { redirect_to @location, notice: "La Location vient d'être créé." }
        format.json { render :show, status: :created, location: @location }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /locations/1 or /locations/1.json
  def update
    respond_to do |format|
      if @location.update(location_params)
        format.html { redirect_to @location, notice: "La Location vient d'être modifié." }
        format.json { render :show, status: :ok, location: @location }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /locations/1 or /locations/1.json
  def destroy
    @location.destroy
    respond_to do |format|
      format.html { redirect_to locations_url, notice: "La Location vient d'être supprimé." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_location
      @location = Location.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def location_params
      params.require(:location).permit(:titre, :nbChambre, :nbCouchage, :desc, :v_type, :address, :street, :zip, :city, :lat, :long, :price, :photos, :avg_rate, :taxesjr)
    end
end
