class FavorisController < ApplicationController
  before_action :authenticate_user!
  before_action :set_favori, only: %i[ show edit update destroy ]

  # GET /favoris or /favoris.json
  def index
    @favoris = Favori.where(user_id: current_user.id)
    @locations = Location.all
  end

  # GET /favoris/1 or /favoris/1.json
  def show
  end

  # GET /favoris/new
  def new
    @favori = Favori.new
  end

  # GET /favoris/1/edit
  def edit
  end

  # POST /favoris or /favoris.json
  def create
    @favori = Favori.new(favori_params)
    @favori.user_id = current_user.id
    @favori.location_id = @favori.location_id
    @location = Location.find(@favori.location_id)

    

    respond_to do |format|

      @favoris_exists = Favori.where(user_id: current_user.id).where(location_id: @favori.location_id)
      if @favoris_exists.count >= 1
        format.html { redirect_to @location, alert: "Cette location est déjà dans vos favoris !" }
        format.json { render :index, status: :unprocessable_entity, location: @location }

      else
        if @favori.save
          format.html { redirect_to @location, notice: "Cette location a été ajoutée aux favoris." }
          format.json { render :index, status: :created, location: @location }
        else
          format.html { render :new, status: :unprocessable_entity }
          format.json { render json: @favori.errors, status: :unprocessable_entity }
        end
      end
      
    end
  end

  # PATCH/PUT /favoris/1 or /favoris/1.json
  def update
    respond_to do |format|
      if @favori.update(favori_params)
        format.html { redirect_to @favori, notice: "Le favoris a été modifié." }
        format.json { render :show, status: :ok, location: @favori }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @favori.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /favoris/1 or /favoris/1.json
  def destroy
    @favori.destroy
    respond_to do |format|
      format.html { redirect_to favoris_url, notice: "L'annonce a été retirée des favoris avec succès" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_favori
      @favori = Favori.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def favori_params
      params.require(:favori).permit(:location_id)
    end
end
