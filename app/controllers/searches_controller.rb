class SearchesController < ApplicationController
  def new
    @search = Search.new
    @v_type = Location.pluck(:v_type).uniq
  end

  def create
    @search = Search.create(search_params)
    redirect_to @search
  end

  def show
    @search = Search.find(params[:id])
  end

  private
  def search_params
    params.require(:search).permit(:keywords, :min_nbChambre, :max_nbChambre, :min_nbCouchage, :max_nbCouchage, :v_type, :zip, :city, :min_price, :max_price, :min_avg_rate, :max_avg_rate, :min_availableDate, :max_availableDate)
  end


end
