class AvisController < ApplicationController
  before_action :authenticate_user!
  before_action :set_avi, only: %i[ show edit update destroy ]

  # GET /avis or /avis.json
  def index
    @avis = Avi.where(user_id: current_user.id)
    @locations = Location.all
  end

  # GET /avis/1 or /avis/1.json
  def show
  end

  # GET /avis/new
  def new
    @avi = Avi.new
  end

  # GET /avis/1/edit
  def edit
  end

  # POST /avis or /avis.json
  def create
    @avi = Avi.new(avi_params)
    @avi.user_id = current_user.id
    @avi.location_id = params[:location_id]

    respond_to do |format|
      if @avi.save

        # RECALCULER LA NOTE MOYENE D'UN LOGEMENT
        @avis_location = Avi.where(location_id: params[:location_id])
        @location = Location.find(params[:location_id])
        @new_avg_rate = 0.0
        @avis_location.each do |avi_location|
          @new_avg_rate = @new_avg_rate + avi_location.note
        end

        if @avis_location.count >= 1
          @location.avg_rate = (@new_avg_rate/@avis_location.count).round(1)
        else
          @location.avg_rate = 11
        end
        
        @location.save

        format.html { redirect_to @location, notice: "Votre avis a été ajouté avec succès" }
        format.json { render :show, status: :created, location: @location }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @avi.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /avis/1 or /avis/1.json
  def update
    respond_to do |format|
      if @avi.update(avi_params)

        # RECALCULER LA NOTE MOYENE D'UN LOGEMENT
        @avis_location = Avi.where(location_id: @avi.location_id)
        @location = Location.find(@avi.location_id)
        @new_avg_rate = 0.0
        @avis_location.each do |avi_location|
          @new_avg_rate = @new_avg_rate + avi_location.note
        end

        if @avis_location.count >= 1
          @location.avg_rate = (@new_avg_rate/@avis_location.count).round(1)
        else
          @location.avg_rate = 11
        end

        @location.save

        format.html { redirect_to @location, notice: "Votre avis a correctement été mis à jour" }
        format.json { render :show, status: :ok, location: @location }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @avi.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /avis/1 or /avis/1.json
  def destroy
    @avi.destroy

    # RECALCULER LA NOTE MOYENE D'UN LOGEMENT
    @avis_location = Avi.where(location_id: @avi.location_id)
    @location = Location.find(@avi.location_id)
    @new_avg_rate = 0.0
    @avis_location.each do |avi_location|
      @new_avg_rate = @new_avg_rate + avi_location.note
    end

    if @avis_location.count >= 1
      @location.avg_rate = (@new_avg_rate/@avis_location.count).round(1)
    else
      @location.avg_rate = 11
    end

    @location.save

    respond_to do |format|
      format.html { redirect_to avis_url, notice: "Votre avis a correctement été supprimé" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_avi
      @avi = Avi.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def avi_params
      params.require(:avi).permit(:note, :comment)
    end
end
