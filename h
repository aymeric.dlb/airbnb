[33mcommit 2082f96d04d966c2a6e125637d93cb75c7cc5b32[m[33m ([m[1;36mHEAD -> [m[1;32mjoffrey[m[33m)[m
Author: Joffrey Girard <joffrey.girard.lpcb@gmail.com>
Date:   Sun Jan 30 21:25:54 2022 +0100

    add reservations

[33mcommit 9f9643231f5665905f8d275665eca92a25edf08e[m[33m ([m[1;31morigin/joffrey[m[33m)[m
Author: Joffrey Girard <joffrey.girard.lpcb@gmail.com>
Date:   Sun Jan 30 20:28:44 2022 +0100

    add favoris

[33mcommit 37a1f3423a8655cd14697d5e135c207049feea3c[m
Author: Joffrey Girard <joffrey.girard.lpcb@gmail.com>
Date:   Sun Jan 30 19:50:25 2022 +0100

    recalculer la note moyenne d'un logement quand on poste un nouvel avis

[33mcommit 42573ca9b20a16e2aec05b2ed736f1e6149bbaad[m
Author: Joffrey Girard <joffrey.girard.lpcb@gmail.com>
Date:   Sat Jan 29 22:44:50 2022 +0100

    set location on create avis

[33mcommit 440f3af56c6a1d7137cdecd7c2e836d643b35cfd[m
Author: Joffrey Girard <joffrey.girard.lpcb@gmail.com>
Date:   Sat Jan 29 21:16:01 2022 +0100

    set current user on create location

[33mcommit 4507383abcc9ed3e2b6d6e3261cd3bd526361aca[m
Author: Joffrey Girard <joffrey.girard.lpcb@gmail.com>
Date:   Sat Jan 29 20:45:25 2022 +0100

    set current user on create avis

[33mcommit e8d0d273eb562c15098df8caf952bcdaf4c9cbae[m
Author: Joffrey Girard <joffrey.girard.lpcb@gmail.com>
Date:   Fri Jan 28 17:45:12 2022 +0100

    create table avis + relation between avis and user

[33mcommit 6c8a3eea938ed10b5e9f03a2dd0219cc413e2028[m[33m ([m[1;31morigin/master[m[33m, [m[1;32mmaster[m[33m)[m
Author: Joffrey Girard <joffrey.girard.lpcb@gmail.com>
Date:   Fri Jan 21 17:07:10 2022 +0100

    add devise

[33mcommit bf92d4a8efed535f19c945ba804a476ea3a9d216[m
Author: Joffrey Girard <joffrey.girard.lpcb@gmail.com>
Date:   Sun Jan 16 18:42:05 2022 +0100

    add bootstrap

[33mcommit 988ee9f104c58c78a426688d6549bc4bb22b631e[m
Author: Joffrey Girard <joffrey.girard.lpcb@gmail.com>
Date:   Fri Jan 7 09:20:37 2022 +0100

    Edit locations form labels

[33mcommit 45de88ef5a605df52013a122a9d8e45f53258458[m
Author: Aymeric <aymeric.delabarre@gmail.com>
Date:   Fri Nov 26 17:06:51 2021 +0100

    Modif location migration

[33mcommit 37f2d5b3b0a72ba8c2b85d00aeadcc123531f1ff[m
Author: Aymeric <aymeric.delabarre@gmail.com>
Date:   Fri Nov 26 16:20:13 2021 +0100

    Ajout Location migrate

[33mcommit 3fec0c3325385c70aeeb8d11970cf5fa5feda380[m
Author: Aymeric <aymeric.delabarre@gmail.com>
Date:   Fri Nov 26 15:21:25 2021 +0100

    initial commit
