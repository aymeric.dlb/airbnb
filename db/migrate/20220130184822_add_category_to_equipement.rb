class AddCategoryToEquipement < ActiveRecord::Migration[6.1]
  def change
    add_reference :equipements, :categories, null: false, foreign_key: true
  end
end
