class EquipementsLocations < ActiveRecord::Migration[6.1]
  def change
    create_join_table :equipements, :locations do |t|
      t.index [:equipement_id, :locations_id]
      t.index [:locations_id, :equipement_id]
    end
    add_foreign_key :equipements_locations, :equipements
    add_foreign_key :equipements_locations, :locations
  end
end
