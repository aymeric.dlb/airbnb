class CreateLocations < ActiveRecord::Migration[6.1]
  def change
    create_table :locations do |t|
      t.string :titre
      t.integer :nbChambre
      t.integer :nbCouchage
      t.text :desc
      t.string :v_type
      t.string :address
      t.string :street
      t.string :zip
      t.string :city
      t.string :lat
      t.string :long
      t.float :price
      t.string :photos
      t.float :avg_rate

      t.timestamps
    end
  end
end
