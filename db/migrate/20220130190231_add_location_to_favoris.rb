class AddLocationToFavoris < ActiveRecord::Migration[6.1]
  def change
    add_reference :favoris, :location, null: false, foreign_key: true
  end
end
