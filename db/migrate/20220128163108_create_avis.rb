class CreateAvis < ActiveRecord::Migration[6.1]
  def change
    create_table :avis do |t|
      t.integer :note
      t.text :comment

      t.timestamps
    end
  end
end
