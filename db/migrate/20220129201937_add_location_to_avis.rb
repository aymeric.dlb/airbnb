class AddLocationToAvis < ActiveRecord::Migration[6.1]
  def change
    add_reference :avis, :location, null: false, foreign_key: true
  end
end
