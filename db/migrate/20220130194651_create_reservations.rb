class CreateReservations < ActiveRecord::Migration[6.1]
  def change
    create_table :reservations do |t|
      t.date :datedeb
      t.date :datefin
      t.integer :nbpersonnes
      t.float :price
      t.string :status

      t.timestamps
    end
  end
end
