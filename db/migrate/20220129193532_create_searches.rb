class CreateSearches < ActiveRecord::Migration[6.1]
  def change
    create_table :searches do |t|
      t.string :keywords
      t.integer :min_nbChambre
      t.integer :max_nbChambre
      t.integer :min_nbCouchage
      t.integer :max_nbCouchage
      t.string :v_type
      t.string :zip
      t.string :city
      t.float :min_price
      t.float :max_price
      t.float :min_avg_rate
      t.float :max_avg_rate
      t.datetime :min_availableDate
      t.datetime :max_availableDate

      t.timestamps
    end
  end
end
