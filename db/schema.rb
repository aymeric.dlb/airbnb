# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_02_03_164958) do

  create_table "avis", force: :cascade do |t|
    t.integer "note"
    t.text "comment"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id", null: false
    t.integer "location_id", null: false
    t.index ["location_id"], name: "index_avis_on_location_id"
    t.index ["user_id"], name: "index_avis_on_user_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "nom"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "conversation_users", force: :cascade do |t|
    t.integer "conversation_id", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["conversation_id"], name: "index_conversation_users_on_conversation_id"
    t.index ["user_id"], name: "index_conversation_users_on_user_id"
  end

  create_table "conversations", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "equipements", force: :cascade do |t|
    t.string "nom"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "categories_id", null: false
    t.integer "location_id"
    t.index ["categories_id"], name: "index_equipements_on_categories_id"
    t.index ["location_id"], name: "index_equipements_on_location_id"
  end

  create_table "equipements_locations", id: false, force: :cascade do |t|
    t.integer "equipement_id", null: false
    t.integer "location_id", null: false
    t.index "\"equipement_id\", \"locations_id\"", name: "index_equipements_locations_on_equipement_id_and_locations_id"
    t.index "\"locations_id\", \"equipement_id\"", name: "index_equipements_locations_on_locations_id_and_equipement_id"
  end

  create_table "favoris", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "location_id", null: false
    t.integer "user_id", null: false
    t.index ["location_id"], name: "index_favoris_on_location_id"
    t.index ["user_id"], name: "index_favoris_on_user_id"
  end

  create_table "locations", force: :cascade do |t|
    t.string "titre"
    t.integer "nbChambre"
    t.integer "nbCouchage"
    t.text "desc"
    t.string "v_type"
    t.string "address"
    t.string "street"
    t.string "zip"
    t.string "city"
    t.string "lat"
    t.string "long"
    t.float "price"
    t.string "photos"
    t.float "avg_rate"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id"
    t.float "taxesjr"
    t.index ["user_id"], name: "index_locations_on_user_id"
  end

  create_table "messages", force: :cascade do |t|
    t.integer "conversation_id", null: false
    t.integer "user_id", null: false
    t.text "body"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["conversation_id"], name: "index_messages_on_conversation_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "reservations", force: :cascade do |t|
    t.date "datedeb"
    t.date "datefin"
    t.integer "nbpersonnes"
    t.float "price"
    t.string "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id", null: false
    t.integer "location_id", null: false
    t.index ["location_id"], name: "index_reservations_on_location_id"
    t.index ["user_id"], name: "index_reservations_on_user_id"
  end

  create_table "searches", force: :cascade do |t|
    t.string "keywords"
    t.integer "min_nbChambre"
    t.integer "max_nbChambre"
    t.integer "min_nbCouchage"
    t.integer "max_nbCouchage"
    t.string "v_type"
    t.string "zip"
    t.string "city"
    t.float "min_price"
    t.float "max_price"
    t.float "min_avg_rate"
    t.float "max_avg_rate"
    t.datetime "min_availableDate"
    t.datetime "max_availableDate"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "avis", "locations"
  add_foreign_key "avis", "users"
  add_foreign_key "conversation_users", "conversations"
  add_foreign_key "conversation_users", "users"
  add_foreign_key "equipements", "categories", column: "categories_id"
  add_foreign_key "equipements", "locations"
  add_foreign_key "equipements_locations", "equipements"
  add_foreign_key "equipements_locations", "locations"
  add_foreign_key "favoris", "locations"
  add_foreign_key "favoris", "users"
  add_foreign_key "locations", "users"
  add_foreign_key "messages", "conversations"
  add_foreign_key "messages", "users"
  add_foreign_key "reservations", "locations"
  add_foreign_key "reservations", "users"
end
